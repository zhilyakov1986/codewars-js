function primeNum() {
  let primeList = [2];
  for (i = 3; i <= 100; i++) {
    for (j = 2; j < i; j++) {
      if (i % j === 0) {
        // skip the number
        continue;
      } else {
        primeList.push(i);
      }
    }
  }
  console.log(primeList);
}
primeNum();
