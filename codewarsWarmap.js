// function century(year) {
//     if (year % 100 === 0) {
//         return year / 100
//     } else {
//         return Math.floor((year) / (100)) + 1
//     }
// }
// top solution
const century = (year) => Math.ceil(year / 100);
// console.log(century(1));

// function sumOfDifferences(arr) {
//     arr.sort((a, b) => b - a);
//     sumOfDiff = []
//     if (arr.length > 1) {
//         for (let i = 0; i < arr.length-1; i++) {
//             sumOfDiff.unshift(arr[i] - arr[i + 1])
//         }
//         return sumOfDiff.reduce((a, b) => a + b)
//     } else {
//         return arr;
//     }
// }

function sumOfDifferences(arr) {
  return arr.length > 1 ? Math.max(...arr) - Math.min(...arr) : 0;
}
// console.log(sumOfDifferences([1, 2, 10]))

//find gravitaitonal force
// solution = (arr_val, arr_unit) => {
//     for (let i=0; i<arr_unit.length; i++){
//         if (arr_unit[i]==='lb'){
//             arr_val[i] *= 0.453592
//         }
//         if (arr_unit[i] === 'μg') {
//             arr_val[i] *= 1e-9
//         }
//         if (arr_unit[i]==='mg'){
//             arr_val[i] *= 1e-6
//         }
//         if (arr_unit[i]==='g'){
//             arr_val[i] *= 0.001
//         }
//         if (arr_unit[i]==='cm'){
//             arr_val[i] *= 0.01
//         }
//         if (arr_unit[i]==='mm'){
//             arr_val[i] *= 0.001
//         }
//         if (arr_unit[i] === 'μm') {
//             arr_val[i] *= 1e-6
//         }
//         if (arr_unit[i]==='ft'){
//             arr_val[i] *= 0.3048
//         }
//     }
//     let force = (6.67e-11*arr_val[0]*arr_val[1])/(arr_val[2]**2)
//     return force
// };
function solution([m1, m2, d], [um1, um2, ud]) {
  const G = 6.67e-11;
  const conversion = {
    kg: 1,
    g: 1e-3,
    mg: 1e-6,
    μg: 1e-9,
    lb: 0.453592,
    m: 1,
    cm: 1e-2,
    mm: 1e-3,
    μm: 1e-6,
    ft: 0.3048,
  };
  return (
    (G * m1 * conversion[um1] * m2 * conversion[um2]) /
    (d * conversion[ud]) ** 2
  );
}
// console.log(solution([1000, 1000, 100], ["kg", "kg", "cm"]));
// function logs(x, a, b) {
//     return (Math.log(a) / Math.log(x)) + (Math.log(b) / Math.log(x))
// }
const logs = (x, a, b) => Math.log(a * b) / Math.log(x);
// console.log(logs(10, 2, 3))
// ternary + arrow func
const bonusTime = (salary, bonus) =>
  bonus ? `\u00A3${salary * 10}` : `\u00A3${salary}`;
// array sort
const fixTheMeerkat = (arr) => [arr[2], arr[1], arr[0]];
// function fixTheMeerkat(arr) {
//     return arr.reverse();
// }
// console.log(fixTheMeerkat(['tail', 'body', 'head']));
const summation = (num) => {
  var sum = 0;
  while (num) {
    sum += num;
    num -= 1;
  }
  return sum;
};
// console.log('summation: ', summation(8));
function expressionMatter(a, b, c) {
  return Math.max(a * (b + c), a * b * c, a + b * c, (a + b) * c, a + b + c);
}
// console.log(expressionMatter(2, 1, 2));

//replacing a letter
//using regex
function DNAtoRNA(dna) {
  // create a function which returns an RNA sequence from the given DNA sequence
  return String(dna).replace(/T/g, 'U');
}
function DNAtoRNAsplit(dna) {
  return dna.split('T').join('U');
}
// console.log(DNAtoRNA('TTTT'));

//fib list
function fibNum(num) {
  if (num === 1 || num === 2) {
    return 1;
  } else {
    return fibNum(num - 1) + fibNum(num - 2);
  }
}
function fibList(num) {
  fib_list = [];
  for (let i = 1; i < num; i++) {
    fib_list.push(fibNum(i));
  }
  return fib_list;
}
// console.log(fibList(10))
function maskify(cc) {
  if (cc.length <= 4) {
    return cc;
  } else {
    let ccList=cc.split('')  
      for (let i = 0; i < ccList.length - 4; i++) {
          ccList.splice(i,1,'#')
    }
      return ccList.join('');
  }
}
// return masked string
function maskifyTop(cc) {
    return cc.replace(/.(?=....)/g, '#');
}
// console.log(maskify('4556364607935616'));

//casting boolean to string
function booleanToString(b) {
    return b.toString();
}
//mixing filter and
function positiveSum(arr) {
    return arr.reduce((a, b) => a + (b > 0 ? b : 0), 0);
}
//findign min element of the array and using spread operator
function findSmallestInt(args) {
    return Math.min(...args);
}
//using abs to always return negative #
function makeNegative(num) {
    return -Math.abs(num);
}
//using parseInt
function fakeBin(x) {
    numList = x.split('');
    for (let i = 0; i < numList.length; i++) {
        if (parseInt(numList[i],10) < 5) {
          numList.splice(i, 1, '0');
        } else {
          numList.splice(i, 1, '1');
        }
    }
    return numList.join('')
}
//top solution
function fakeBin(x) {
  return x
    .split('')
    .map((n) => (n < 5 ? 0 : 1))
    .join('');
}
// using regex
function fakeBin(x) {
    return x.replace(/\d/g, d => d < 5 ? 0 : 1);
}
console.log(fakeBin('45385593107843568'))