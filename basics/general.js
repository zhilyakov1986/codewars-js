// using ternary operator
let howManyLightsabersDoYouOwn = name => (name == 'Zach' ? 18 : 0);

let enough = (cap, on, wait) => (cap < on + wait ? on + wait - cap : 0);

// function strCount(str, letter){
//     //code here
//     let counter = 0;

//     for (let i = 0; i<str.length; i++){
//       if(str[i]===letter){
//         counter++;
//         }
//      }
//      return counter;
//   }

function strCount(str, letter) {
  return str.split('').filter(c => c == letter).length;
}

function validParentheses(parens) {
  let parensArray = parens.split('');
  if (parens.length % 2 !== 0 || parensArray[0] === ')') {
    return false;
  }
  let counter = 0;
  for (let i = 0; i < parensArray.length; i++) {
    if (parensArray[i] === '(') {
      counter++;
    }

    if (parensArray[i] === ')') {
      counter--;
    }
    if (counter < 0) {
      return false;
    }
  }
  if (counter === 0) {
    return true;
  } else {
    return false;
  }
}

console.log(validParentheses('(())((()())())'));
