function longestEvenWord(sentence) {
    let listSentence = sentence.split(' ');
    let longestEven = '';
    for (let i = 0; i < listSentence.length; i++) {
        if (listSentence[i].length % 2 == 0 && listSentence[i].length > longestEven.length) {
            longestEven = listSentence[i];
        }
    }
    return longestEven;

}
// console.log(longestEvenWord('It is a pleasant day today'));
function numberOfPairs(a, k) {
    {
        let distinctPair = [];
        let listOfPairs = [];
        // Pick all elements one by one
        for (let i = 0; i < a.length; i++) {
            // See if there is a pair
            // of this picked element
            for (let j = i + 1; j < a.length; j++)
                if (a[i] + a[j] == k ||
                    a[j] + a[i] == k)
                    distinctPair = [a[i], a[j]];
                
            listOfPairs.push(distinctPair);
        }
        let setUniquePairs = new Set(listOfPairs);
        let arrayUniquePairs = [...setUniquePairs];
        filteredUniquePairs = arrayUniquePairs.filter(a);
        return filteredUniquePairs;
    }

}
console.log(numberOfPairs([1, 3, 46, 1, 3, 9], 47))